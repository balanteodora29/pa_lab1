package pa.lab1.compulsory;

public class Compulsory {

    /**
     * Functia returneaza un numar intreg ales aleatoriu din intervalul [0, 1_000_000).
     */
    private static int genRandomNr(){
        return (int) (Math.random() * 1_000_000);
    }

    /**
     * Pentru numarul intreg dat ca parametru, n, functia va executa urmatoarele:
     * - inmulteste n cu 3;
     * - la rezultat adauga numarul binar 10101;
     * - la rezultat adauga numarul hexadecimal FF;
     * - inmulteste rezultatul cu 6;
     * Returneaza rezultatul.
     */
    private static int calculations(int n){
        n *= 3;
        n += 0b10101;
        n += 0xFF;
        n *= 6;
        return n;
    }

    /**
     * Functia sumDigits returneaza suma cifrelor numarului dat ca parametru.
     *
     * Suma cifrelor unui numar este egala cu numarul modulo 9.
     * Exceptie fac numerele divibile cu 9, care modulo 9 dau 0.
     * Pentru numerele divizibile cu 9 vom returna 9.
     *
     * Explicatia matematica: https://www.sjsu.edu/faculty/watkins/Digitsum00.htm
     */
    private static int sumDigits(int number){
        if (number % 9 == 0) {
            return 9;
        }
        return number % 9;
    }

    public static void main(String[] args) {
        System.out.println("Hello world!");

        String[] languages = {"C", "C++", "C#", "Python", "Go", "Rust", "JavaScript", "PHP", "Swift", "Java"};

        int n = genRandomNr();
        System.out.println("Numarul generat random: " + n);

        n = calculations(n);
        n = sumDigits(n);

        System.out.println("Rezultatul obtinut: " + n);

        System.out.println("Willy-nilly, this semester I will learn " + languages[n] + ".");
    }
}
