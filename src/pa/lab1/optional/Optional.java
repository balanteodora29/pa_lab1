package pa.lab1.optional;

public class Optional {

    /**
     * @param startTime - retains the time from the moment we run the program.
     * The function displays the time in nanoseconds.
     * We subtract from the current time, the time retained in startTime.
     */
    public static void displayTime(long startTime){
        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("Time in nanoseconds: " + totalTime);
    }

    /**
     * The function returns an adjacency matrix (size number x number) that represents a random graph.
     * For each position in the matrix we will randomly assign 0 or 1
     * and we will also assign the value to the symmetric position (the adjacency matrix is symmetrical).
     */
    private static int[][] createAdjMtx(int number){
        int[][] adjMtx = new int[number][number];

        for(int i=0; i<number; i++) {
            for (int j=0; j < number; j++) {
                if (i != j) {
                    adjMtx[i][j] = (int) (Math.random() * 2);
                    adjMtx[j][i] = adjMtx[i][j];
                }
            }
        }
        return adjMtx;
    }

    /**
     * The function displays an array received as a parameter.
     */
    private static void displayMtx(int[][] adjMtx){
        for(int i=0; i<adjMtx.length; i++) {
            for (int j = 0; j < adjMtx.length; j++) {
                System.out.print(adjMtx[i][j] + "  ");
            }
            System.out.print("\n");
        }
    }

    /**
     * The function displays an adjacency matrix received as a parameter using Unicode symbols.
     */
    private static void displayMtxUnicode(int[][] adjMtx){
        for(int i=0; i<adjMtx.length; i++) {
            for (int j = 0; j < adjMtx.length; j++) {
                if (adjMtx[i][j] == 0)
                    System.out.print("\u25CB" + " ");
                else
                    System.out.print("\u25A0" + " ");
            }
            System.out.print("\n");
        }
    }

    /**
     * The function traverses the graph in DFS mode starting from the node that is given as a parameter.
     * Returns the visitor vector.
     * @param nod - the node from which the traversal of the graph begins;
     * @param adjMtx - graph adjacency matrix;
     * @param visited - a vector that holds for each node whether it has been visited (1) or not (0);
     */
    public static int[] visitDFS(int nod, int[][] adjMtx, int[] visited){
        visited[nod]=1;

        for (int j=0; j< adjMtx.length; j++){
            if (adjMtx[nod][j] == 1 && visited[j]==0 && nod!=j){
                visitDFS(j, adjMtx, visited);
            }
        }
        return visited;
    }

    /**
     * The function checks if a graph is connected and returns true or false.
     * A graph is connected if by traversing the graph starting from a random node, you can reach any other node in the graph.
     * If there is a value of 0 in the visitor vector, it means that this graph is not connected.
     * @param adjMtx - graph adjacency matrix;
     * @param visited - a vector that holds for each node whether it has been visited (1) or not (0);
     */
    public static boolean isConnected(int[][] adjMtx, int[] visited){
        visited = visitDFS(0, adjMtx, visited);
        for (int i=0; i<visited.length; i++) {
            if (visited[i] == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * The function displays the connected components of a graph.
     * The first time will display the component that was retained in the visitor vector starting from node 0;
     * Then, while we have unvisited nodes, we will go through the graph and recursively display the remaining components;
     * @param adjMtx - graph adjacency matrix;
     * @param visited - a vector that holds for each node whether it has been visited (1) or not (0);
     */
    public static void displayComponents(int[][] adjMtx, int[] visited){
        System.out.print("{ ");
        for (int i=0; i<visited.length; i++){
            if (visited[i]==1){
                System.out.print(i + " ");
                visited[i]++;
            }
        }
        System.out.print("} ");

        for (int i=0;i<visited.length; i++){
            if (visited[i]==0){
                visited = visitDFS(i, adjMtx, visited);
                displayComponents(adjMtx, visited);
            }
        }
    }

    /**
     * The function creates an adjacency matrix that represents a random partially tree made after a graph.
     * The matrix is made by traversing in mode DFS the graph.
     * @param nod - the node in which we are at an iteration (initially it will be the root);
     * @param adjMtx - graph adjacency matrix;
     * @param visited - a vector that holds for each node whether it has been visited (1) or not (0);
     * @param adjMtxTree - tree adjacency matrix;
     */
    public static void createPartTreeDFS(int nod, int[][] adjMtx, int[] visited, int[][] adjMtxTree){
        visited[nod]=1;
        for (int j=0; j< adjMtx.length; j++){
            if (adjMtx[nod][j] == 1 && visited[j]==0 && nod!=j){
                adjMtxTree[nod][j] = adjMtxTree[j][nod] = 1;
                createPartTreeDFS(j, adjMtx, visited, adjMtxTree);
            }
        }
    }

    /**
     * The function displays the adjacent matrix of a partially tree created based on a given graph as a parameter.
     * The root of the tree is chosen randomly;
     * @param adjMtx - the adjacency matrix of the graph after which we make the partial tree;
     */
    public static void createPartTree(int[][] adjMtx){
        int root = (int) (Math.random() * adjMtx.length);
        System.out.println("The root: " + root); System.out.println();
        int[] visited = new int[adjMtx.length];
        int[][] adjMtxTree = new int[adjMtx.length][adjMtx.length];
        createPartTreeDFS(root, adjMtx, visited, adjMtxTree);
        displayMtx(adjMtxTree);
    }


    public static void main(String[] args) {
        long startTime = System.nanoTime();

        int number = Integer.parseInt(args[0]); // the number given on the command line
        System.out.println("The number is: " + number); System.out.println();

        if (number > 30_000){
            displayTime(startTime);
        }
        else{
            int[][] adjMtx = createAdjMtx(number); // we construct the graph in the form of an adjacent matrix
            System.out.println("The graph with " + number + " nodes: ");
            displayMtx(adjMtx); System.out.println();
            // displayMtxUnicode(adjMtx); System.out.println();

            int[] visited = new int[adjMtx.length];

            boolean answer = isConnected(adjMtx, visited); // we check if the graph is connected
            System.out.println("Is the graph connected? -> " + answer); System.out.println();

            if (!answer){
                System.out.println("The connected components of the graph: ");
                displayComponents(adjMtx, visited); // if the graph is not connected, we display the components
            }
            else{
                System.out.println("The partially tree: ");
                createPartTree(adjMtx); // if the graph is connected, we make the partially tree
            }
        }
    }
}
