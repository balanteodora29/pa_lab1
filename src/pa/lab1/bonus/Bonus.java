package pa.lab1.bonus;

public class Bonus {

    /**
     * The function returns a random sequence of n elements;
     * @param n - the number of elements in the sequence;
     * @param seq -  the sequence;
     */
    static public int[] randomSequence(int n, int[] seq){
        for (int i=0; i<n; i++){
            seq[i]= (int) (Math.random() * n);
        }
        return seq;
    }

    /**
     * The function creates a tree with n+2 nodes and no edges (the degree of each node is 0);
     * @param n - the number of elements in the sequence;
     * @param tree - the constructed tree;
     * @param degree - an array that keeps the degree for each node in the tree;
     */
    static public int[] initTree(int n, int[] tree, int[] degree){
        for (int i=0; i<n+2; i++){
            tree[i]= i;
            degree[i] = 1;
        }
        return tree;
    }

    /**
     * The function calculates the degree for each node based on the number of appearences in the sequence;
     * @param seq -  the sequence;
     * @param tree - the constructed tree;
     * @param degree - an array that keeps the degree for each node in the tree;
     */
    static public void calcNodeDegree(int[] seq, int[] tree, int[] degree){
        for (int i=0; i<seq.length; i++){
            degree[seq[i]]++;
        }
    }

    /**
     * The function adds the edges in the adjency matrix by means of the sequence;
     * The last two nodes with the degree 0 will have an edge between them;
     * @param seq -  the sequence;
     * @param tree - the constructed tree;
     * @param degree - an array that keeps the degree for each node in the tree;
     * @param adjMtx - the adjency matrix of the tree;
     */
    static public void addEdges(int[] seq, int[] tree, int[] degree, int[][] adjMtx){
        for (int i=0; i<seq.length; i++){
            for (int j=0; j<tree.length; j++){
                if (degree[j] == 1 && seq[i]!=j){
                    adjMtx[seq[i]][j] = adjMtx[j][seq[i]] = 1;
                    degree[seq[i]]--;
                    degree[j]--;
                    break;
                 }
            }
        }

        System.out.println("The random sequence: ");
        for(int i=0; i<seq.length; i++) {
            System.out.print(seq[i] + ",  " + degree[i] + " || ");
        }
        System.out.println();

        int node1 = -1, node2 = -1;
        for (int i=0; i<tree.length; i++) {
            if (degree[i] == 0 ) {
                if (node1 == -1) {
                    node1 = i;
                } else {
                    if (node2 == -1) {
                        node2 = i;
                    }
                    else break;
                }
            }

        }
        System.out.println("Nodurile ramase:" + node1 + " " + node2);
        adjMtx[node1][node2] = adjMtx[node2][node1] = 1;
        degree[node1]--;
        degree[node2]--;
    }

    /**
     * The function displays an matrix given as a parameter;
     * @param adjMtx - the adjency matrix of the tree;
     */
    private static void displayMtx(int[][] adjMtx){
        System.out.println("The adjacency matrix: ");
        for(int i=0; i<adjMtx.length; i++) {
            for (int j = 0; j < adjMtx.length; j++) {
                System.out.print(adjMtx[i][j] + "  ");
            }
            System.out.print("\n");
        }
    }

    static public void main(String[] args) {
        int n = Integer.parseInt(args[0]);

        int[] seq = new int[n];
        seq = randomSequence(n, seq);
        System.out.println("The random sequence: ");
        for(int i=0; i<seq.length; i++) {
            System.out.print(seq[i] + "  ");
        }
        System.out.println();

        int[] tree = new int[n+2];
        int[] degree = new int[n+2];
        tree = createTree(n, tree, degree);
        calcNodeDegree(seq, tree, degree);

        int[][] adjMtx = new int[n+2][n+2];
        addEdges(seq, tree, degree, adjMtx);

        displayMtx(adjMtx);
    }
}