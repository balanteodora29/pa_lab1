# Laboratorul 1

[Link cerinta](https://profs.info.uaic.ro/~acf/java/labs/lab_01.html)

## Compulsory

Write a Java application that implements the following operations:  
1. Display on the screen the message "Hello World!". Run the application. If it works, go to step 2 :)
2. Define an array of strings languages, containing {"C", "C++", "C#", "Python", "Go", "Rust", "JavaScript", "PHP", "Swift", "Java"}
3. Generate a random integer n: int n = (int) (Math.random() * 1_000_000);
4. Compute the result obtained after performing the following calculations:
    * multiply n by 3;
    * add the binary number 10101 to the result;
    * add the hexadecimal number FF to the result;
    * multiply the result by 6;
5. Compute the sum of the digits in the result obtained in the previous step. This is the new result. While the new result has more than one digit, continue to sum the digits of the result.
6. Display on the screen the message: "Willy-nilly, this semester I will learn " + languages[result].

## Rezolvare Compulsory

Avem 3 functii:  
* `private static int genRandomNr()` returneaza un numar intreg ales aleatoriu din intervalul [0, 1_000_000];
* `private static int calculations(int n)` metoda executa calculele de la punctul 4 asupra parametrului n;
* `private static int sumDigits(int number)` metoda returneaza suma cifrelor pentru un numar dat ca parametru.

Observam ca indiferent de numarul generat la punctul 3, facand pasii de la punctele 4 si 5 vom obtine de fiecare data numarul 9.  
Mesajul afisat pe ecran va fi de fiecare data: `Willy-nilly, this semester I will learn Java.`

## Optional

1. Let n be an integer given as a command line argument. Validate the argument!
2. Create a n x n matrix, representing the adjacency matrix of a random graph .
3. Display on the screen the generated matrix (you might want to use the geometric shapes from the Unicode chart to create a "pretty" representation of the matrix).
4. Verify if the generated graph is connected and display the connected components (if it is not).
5. Assuming that the generated graph is connected, implement an algorithm that creates a partial tree of the graph. Display the adjacency matrix of the tree.
6. For larger n display the running time of the application in nanoseconds (DO NOT display the matrices). Try n > 30_000. You might want to adjust the JVM Heap Space using the VM options -Xms4G -Xmx4G.
7.  Launch the application from the command line, for example: `java Lab1 100`.

## Rezolvare Optional

Functiile implementate:
* `public static void afisTime(long startTime)` functia afiseaza timpul de rulare a programului in nanosecunde;
* `private static int[][] createAdjMtx(int number)` functia returneaza o matrice de adicenta aleatorie;
* `private static void afisMtx(int[][] adjMtx)` functia afiseaza o matrice de adiacenta primita ca si parametru;
* `private static void afisMtxUnicode(int[][] adjMtx)` functia afiseaza, folosind simboluri Unicode, o matrice de adiacenta primita ca si parametru;
* `public static int[] visitDFS(int nod, int[][] adjMtx, int[] visited)` functia parcurge in mod DFS un graf;
* `public static boolean isConnected(int[][] adjMtx, int[] visited)` functia verifica daca un graf este conectat si returneza true sau false;
* `public static void afisComponents(int[][] adjMtx, int[] visited)` functia afiseaza componentele conexe ale unui graf;
* `public static void createPartTreeDFS(int nod, int[][] adjMtx, int[] visited, int[][] adjMtxTree)` functia creaza o matrice de adiacenta pentru un arbore partial aleatoriu. Face asta prin parcurgerea DFS a grafului dupa care se construieste arborele;
* `public static void createPartTree(int[][] adjMtx)` functia returneaza o matrice de adiacenta care reprezinta un arbore partial aleatoriu creat dupa un graf dat ca parametru;

- Ce se afiseaza in cazul in care graful generat este conectat:
  ![Connected Graph](connectedGraph.PNG)
- Ce se afiseaza in cazul in care graful generat nu este conectat:
  ![Not Connected Graph](notConnectedGraph.PNG)

## Bonus

* Implement an efficient algorithm that generates a random rooted tree. 
* Create and display a textual representation of the tree, for example:  
  ![Exemplu arbore](Bonus.PNG)


## Rezolvare Bonus

Pentru a genera un arbore random m-am folosit de o [secventa Prüfer](https://en.wikipedia.org/wiki/Pr%C3%BCfer_sequence) .  
  
Functiile implementate:  
* `static public int[] randomSequence(int n, int[] seq)` functia returneaza o secventa aleatorie de numere din intervalul [0, n);
* `static public int[] initTree(int n, int[] tree, int[] degree)` functia initializeaza un vector ca va reprezenta nodurile unui arbore, numerotate de la 0 la n+1. Initializeaza gradul fiecarui nod cu valoarea 1;
* `static public void calcNodeDegree(int[] seq, int[] tree, int[] degree)` functia calculeaza gradul fiecarui nod in functie de numarul de aparitii in seceventa;
* `static public void addEdges(int[] seq, int[] tree, int[] degree, int[][] adjMtx)` functia completeaza matricea de adiacenta cu valori de 1 pentru nodurile care au muchie intre ele. Pentru fiecare valoare x din secventa trasam o muchie intre x si primul nod y care nu se regaseste in secventa. Facand acest lucru vor ramane doua noduri izolate si trasam muchie intre ele;
* `private static void displayMtx(int[][] adjMtx)` functia afiseaza matricea de adiacenta a grafului.
